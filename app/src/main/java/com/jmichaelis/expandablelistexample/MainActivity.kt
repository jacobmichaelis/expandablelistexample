package com.jmichaelis.expandablelistexample

import android.database.DataSetObserver
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import androidx.core.content.res.ResourcesCompat
import com.jmichaelis.expandablelistexample.databinding.ActivityMainBinding
import com.jmichaelis.expandablelistexample.databinding.ChildViewBinding
import com.jmichaelis.expandablelistexample.databinding.GroupViewBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        binding.expandableListView.setAdapter(MainAdapter(testDataOrder, testData))
        binding.expandableListView.setGroupIndicator(null)
    }

    companion object {
        val testDataOrder = listOf("Group 1", "Group 2")
        val testData = mapOf(
            "Group 1" to listOf("Lorem ipsum dolor sit amet","consectetur adipiscing elit","Aliquam sed arcu id sapien sodales bibendum","Vivamus metus est","malesuada vitae viverra at","aliquam tristique eros","Praesent viverra ac ipsum bibendum interdum","Ut ipsum neque","euismod ac volutpat vitae","tempor quis nunc","Phasellus in lobortis nunc","Quisque elementum vitae ipsum eu blandit","Curabitur vestibulum tincidunt tristique","Ut faucibus fermentum velit","vitae commodo sapien interdum vitae","Class aptent taciti sociosqu ad litora torquent per conubia nostra","per inceptos himenaeos","Etiam lacus nisi","suscipit at lacus ac","tristique mattis neque","Cras mollis velit quis libero sagittis suscipit","Suspendisse dapibus ex placerat ipsum dictum","quis mattis neque tristique"),
            "Group 2" to listOf("Lorem ipsum dolor sit amet","consectetur adipiscing elit","Aliquam augue nunc, sodales vel ex ut","faucibus lacinia tellus","Cras id eros ut dolor sagittis varius sit amet non leo","Mauris pharetra magna ligula","Integer euismod lacus a efficitur lacinia","Aliquam elementum eu metus nec porta","Quisque molestie lacus felis","in sollicitudin ex dignissim sed","Duis quis magna id nulla lacinia feugiat id venenatis quam","Donec nec urna massa")
        )
    }
}

class MainAdapter(
    private val groupOrder: List<String>,
    private val groups: Map<String, List<String>>
): ExpandableListAdapter {
    override fun getGroupCount(): Int = groupOrder.size
    override fun getGroup(group: Int): Any = groupOrder[group]
    override fun getGroupView(group: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        return LayoutInflater.from(parent?.context).inflate(R.layout.group_view, parent, false).apply {
            val binding = GroupViewBinding.bind(this)
            binding.groupText.text = groupOrder[group]
            binding.groupDropdown.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    if (isExpanded) R.drawable.chevron_up else R.drawable.chevron_down,
                    null
                )
            )
        }
    }

    override fun getChildrenCount(group: Int): Int = groups[groupOrder[group]]!!.size
    override fun getChild(group: Int, child: Int): Any = groups[groupOrder[group]]!![child]
    override fun getChildView(group: Int, child: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        return LayoutInflater.from(parent?.context).inflate(R.layout.child_view, parent, false).apply {
            val binding = ChildViewBinding.bind(this)
            binding.childText.text = groups[groupOrder[group]]!![child]
        }
    }

    override fun getGroupId(group: Int): Long = group.toLong()
    override fun getCombinedGroupId(group: Long): Long = group
    override fun getChildId(group: Int, child: Int): Long = (group * child).toLong()
    override fun getCombinedChildId(group: Long, child: Long): Long = group * child
    override fun hasStableIds(): Boolean = false
    override fun isChildSelectable(group: Int, child: Int): Boolean = false
    override fun areAllItemsEnabled(): Boolean = false
    override fun isEmpty(): Boolean = groupOrder.isEmpty() || groups.values.isEmpty()

    override fun onGroupExpanded(group: Int) {}
    override fun onGroupCollapsed(group: Int) {}
    override fun registerDataSetObserver(p0: DataSetObserver?) {}
    override fun unregisterDataSetObserver(p0: DataSetObserver?) {}
}